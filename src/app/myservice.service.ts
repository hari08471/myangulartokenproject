import { Injectable } from '@angular/core';
import {HttpClient, HttpParams } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class MyserviceService {

  constructor(private http: HttpClient) { }    
    
    
  postData(data: any): any {      
    const body = new HttpParams()          
    .set('grant_type', data.grant_type)          
    .set('username', data.username)    
    .set('password', data.password)    
    return this.http.post('http://localhost:57083/token', body.toString(), {observe: 'response',    
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },    
    });    
  }    
}
