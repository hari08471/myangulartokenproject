import { BrowserModule } from '@angular/platform-browser';  
import { NgModule } from '@angular/core';  
  
import { AppComponent } from './app.component';  
import { LoginComponent } from './login/login.component';  
 
  
import {FormsModule,ReactiveFormsModule } from '@angular/forms'  
import {MyserviceService} from './myservice.service'  
import { HttpClientModule } from '@angular/common/http';  
  
  
  
@NgModule({  
  declarations: [  
    AppComponent,  
    LoginComponent  
  ],  
  imports: [  
    BrowserModule,  
    FormsModule,  
    ReactiveFormsModule ,  
    HttpClientModule  
  ],  
  providers: [MyserviceService],  
  bootstrap: [AppComponent]  
})  
export class AppModule { }