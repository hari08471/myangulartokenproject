import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MyserviceService } from '../myservice.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form!: FormGroup;
  successmsg: any;  
  errmsg: any;  
  constructor(private Userservice : MyserviceService) { }  
  ngOnInit() {  
    this.form = new FormGroup({  
      username: new FormControl('', [Validators.required]),   
      password: new FormControl('', [Validators.required, Validators.minLength(6)]),  
      grant_type: new FormControl('password'),  
     });  
    }  
  
  
onSubmit()  
  {     
      this.Userservice.postData(this.form.value)  
                     .subscribe((res: { status: string | number; body: { access_token: string; }; statusText: string; }) => {    
                       if (res.status === 200) { 
                        this.successmsg = 'token - ' + res.body.access_token;                                                                                     localStorage.setItem('access_token', res.body.access_token);  
                        } else {  
                          this.errmsg = res.status + ' - ' + res.statusText;  
                          }  
                         },  
                       (                       err: { status: number; }) => {                                 
                        if (err.status === 401  ) {  
                          this.errmsg = 'Invalid username or password.';  
                           }   
                          else if (err.status === 400  ) {  
                           this.errmsg = 'Invalid username or password.';  
                          }   
                          else {  
                          this.errmsg ="Invalid username or password";  
                           }  
                        });  
        }   

}
